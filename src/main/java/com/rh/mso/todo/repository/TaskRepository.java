package com.rh.mso.todo.repository;

import com.rh.mso.todo.domain.Task;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface TaskRepository extends CrudRepository<Task, Long> {

    List<Task> findAll();
    List<Task> findByDescriptionContainsAndCategoryContains(String description, String category);
}
