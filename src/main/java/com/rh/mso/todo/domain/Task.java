package com.rh.mso.todo.domain;

import javax.persistence.*;
import java.util.Calendar;
import java.util.Date;


@Entity
@Table(name = "task")
public class Task {

    @Id
    @GeneratedValue
    private Long id;

    private String description;
    private String category;
    private Date execTime;
    private boolean completed;

    @Transient
    private boolean overdue;

    public Task() {
    }

    public Task(String description, String category, Date execTime) {
        this.description = description;
        this.category = category;
        this.setExecTime(execTime);
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Date getExecTime() {
        return execTime;
    }

    public void setExecTime(Date execTime) {
        this.execTime = execTime;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    public boolean isOverdue() {
        if(execTime!=null && execTime.before(Calendar.getInstance().getTime())){
            return true;
        }
        return overdue;
    }
}
