package com.rh.mso.todo.service;

import com.rh.mso.todo.domain.Task;

import java.util.List;

public interface TaskService {
    List<Task> findAll();
    List<Task> searchTask(String description, String category);
    void createTask(Task task);
}

