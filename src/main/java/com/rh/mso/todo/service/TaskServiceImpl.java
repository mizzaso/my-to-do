package com.rh.mso.todo.service;

import com.rh.mso.todo.domain.Task;
import com.rh.mso.todo.repository.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class TaskServiceImpl implements TaskService {

    @Autowired
    private TaskRepository taskRepository;


    @Override
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    public List<Task> searchTask(String description, String category) {
        return taskRepository.findByDescriptionContainsAndCategoryContains(description, category);
    }

    @Override
    public void createTask(Task task) {
        taskRepository.save(task);
    }
}
