package com.rh.mso.todo.web;

import com.rh.mso.todo.domain.Task;
import com.rh.mso.todo.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("my-to-do")
public class TaskController {

    @Autowired
    TaskService taskService;


    @RequestMapping(method = RequestMethod.GET, produces = {"application/json"}, path ="/tasks")
    public List<Task> tasks(){
       return taskService.findAll();
    }

    @RequestMapping(method = RequestMethod.GET, produces = {"application/json"}, path ="/filter_tasks")
    public List<Task> filterTasks(@RequestParam(name="description", required=false, defaultValue = "") String description,
                                  @RequestParam(name="category" ,required=false, defaultValue = "") String category){

        return taskService.searchTask(description, category);
    }

    @PutMapping("/task")
    public String createTask(@Valid @RequestBody Task task){
         taskService.createTask(task);
         return "task created";
    }
}
