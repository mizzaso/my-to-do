package com.rh.mso.todo.repository;

import com.rh.mso.todo.domain.Task;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TaskRepositotyTest {

    @Autowired
    private TaskRepository taskRepository;

    @Before
    public void setUp(){
        for(int c=0;c<10;c++){
            taskRepository.save(new Task("TaskDesc".concat(String.valueOf(c)),"Test",new Date()));
        }
    }

    @Test
    public void test_find_all(){
        List<Task> tasks = taskRepository.findAll();
        Assert.assertEquals(10, tasks.size());
    }

    @Test
    public void test_find_by_filters(){
        List<Task> tasksBothFilters = taskRepository.findByDescriptionContainsAndCategoryContains("0", "es");
        Assert.assertEquals(1, tasksBothFilters.size());

        List<Task> tasksJustCategory = taskRepository.findByDescriptionContainsAndCategoryContains("", "es");
        Assert.assertEquals(10, tasksJustCategory.size());

        List<Task> taskNonFilter = taskRepository.findByDescriptionContainsAndCategoryContains("", "");
        Assert.assertEquals(10, taskNonFilter.size());
    }
}
