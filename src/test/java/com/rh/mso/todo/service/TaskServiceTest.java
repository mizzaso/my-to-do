package com.rh.mso.todo.service;

import com.rh.mso.todo.domain.Task;
import com.rh.mso.todo.repository.TaskRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TaskServiceTest {

    @Autowired
    private TaskService taskService;

    @MockBean
    private TaskRepository taskRepository;

    private List<Task> taskList = new ArrayList<>();

    @Before
    public void setUp(){
        for(int c=0;c<10;c++){
            taskList.add(new Task("TaskDesc".concat(String.valueOf(c)),"Test",new Date()));
        }
    }

    @Test
    public void test_find_all(){
        when(taskRepository.findAll()).thenReturn(taskList);
        List<Task> tasks = taskService.findAll();
        Assert.assertNotNull(tasks);
        Assert.assertEquals(10, tasks.size());
    }
}
