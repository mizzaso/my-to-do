package com.rh.mso.todo.web;

import com.rh.mso.todo.domain.Task;
import com.rh.mso.todo.service.TaskService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class TaskControllerTest {

    @MockBean
    private TaskService taskService;

    @Autowired
    private TestRestTemplate testRestTemplate;


    @Test
    public void test_tasks_return_list() throws Exception {
        List<Task> tasks = new ArrayList<>();
        tasks.add(new Task("WakeUp!", "atHome", new Date()));

        when(taskService.findAll()).thenReturn(tasks);
        ResponseEntity<List> responseEntity = this.testRestTemplate.getForEntity(UriComponentsBuilder
                .fromPath("/my-to-do/tasks").build().toUri(), List.class);

        assertNotNull(responseEntity);
        assertEquals(responseEntity.getStatusCode(), HttpStatus.OK);
        assertEquals(responseEntity.getBody().size(), 1);
    }

    @Test
    public void test_tasks_empty_list() throws Exception {
        when(taskService.findAll()).thenReturn(new ArrayList<>());
        ResponseEntity<List> responseEntity = this.testRestTemplate.getForEntity(UriComponentsBuilder
                .fromPath("/my-to-do/tasks").build().toUri(), List.class);

        assertNotNull(responseEntity);
        assertEquals(responseEntity.getStatusCode(), HttpStatus.OK);
        assertEquals(responseEntity.getBody().size(), 0);
    }

    @Test
    public void test_filtertask_with_filter() throws Exception {
        UriComponentsBuilder uriComponentsBuilder = UriComponentsBuilder.fromPath("/my-to-do/filter_tasks")
                .queryParam("description", "T")
                .queryParam("category","wo");

        ResponseEntity<List> responseEntity = this.testRestTemplate.getForEntity(uriComponentsBuilder.build().toUri(), List.class);
        verify(taskService, times(1)).searchTask("T","wo");
        assertEquals(responseEntity.getStatusCode(), HttpStatus.OK);
    }

    @Test
    public void test_filtertask_without_filter() throws Exception {

        UriComponentsBuilder uriComponentsBuilder = UriComponentsBuilder.fromPath("/my-to-do/filter_tasks");
        ResponseEntity<List> responseEntity = this.testRestTemplate.getForEntity(uriComponentsBuilder.build().toUri(), List.class);
        verify(taskService, times(1)).searchTask("","");
        assertEquals(responseEntity.getStatusCode(), HttpStatus.OK);
    }
}
