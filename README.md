This is a restful API application for manage TO-DO tasks. 

Technology used: Java8, SprintBoot, HsqlDB

Endpoints description: 

- tasks: List all tasks.
    + endpoint : http://{host}/my-to-do/tasks
    + Http Method : GET
    + Json return example: 
                        [
                            {
                                "description": "This is my description",
                                "category": "Third",
                                "execTime": "2019-01-04T17:36:36.520+0000",
                                "completed": true,
                                "overdue": true
                            },
                            {
                                "description": "Second Task",
                                "category": "Work",
                                "execTime": "2019-01-04T17:36:36.520+0000",
                                "completed": false,
                                "overdue": true
                            }
                        ]
- filter_tasks: Filter task list by category and description.
    + endpoint : http://{host}/my-to-do/filter_tasks?category={param1}T&description={param2}
    + Http Method : GET
    + Json return example: 
                        [
                            {
                                "description": "This is my description",
                                "category": "Third",
                                "execTime": "2019-01-04T17:36:36.520+0000",
                                "completed": true,
                                "overdue": true
                            }
                        ]
- task: Use this for create a new task.
    + endpoint : http://{host}/my-to-do/task
    + Http Method : PUT
    + Json Input example: 
                        {
                            "description" : "This is my description",
                            "category" : "Third",
                            "execTime" : "2019-01-04T17:36:36.520+0000",
                            "completed" : true
                        }


** Test coverage of main functionalities.

